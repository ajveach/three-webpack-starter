import {
  Scene,
  PerspectiveCamera,
  WebGLRenderer,
  PointLight,
  MeshLambertMaterial,
} from 'three';

// Add dev css if running in dev environment.
if (process.env.NODE_ENV === 'development') {
  require('../styles/main.dev.css');
}

import '../styles/main.css';
import { RoundedEdgedBox } from './models';

const { innerHeight: height, innerWidth: width } = window;

const scene = new Scene();

const camera = new PerspectiveCamera(55, width / height, 1, 20000);
camera.position.set( 30, 30, 100 );

const renderer = new WebGLRenderer();
const wrapperElement = document.getElementById('cbf3d');

renderer.setSize(width, height);
wrapperElement.appendChild(renderer.domElement);

// Create rounded box
const roundedCube = new RoundedEdgedBox(new MeshLambertMaterial({ color: 0xcd3a27 }), 20, 20, 20, 2, 10, 10, 10, 2);
roundedCube.addTo(scene);

// Create point light.
const light = new PointLight(0xffffff, 10, 30);
light.position.set(0, 30, 0);
scene.add(light);

window.addEventListener('resize', () => {
  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();
  renderer.setSize( window.innerWidth, window.innerHeight);
}, false);

function animate() {
  requestAnimationFrame(animate);

  render();
};

function render() {
  var time = performance.now() * 0.001;

  roundedCube.mesh.rotation.x = time * 0.5;
  roundedCube.mesh.rotation.z = time * 0.51;

  renderer.render(scene, camera);
}

animate();