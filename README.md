# THREE + Webpack Starter

This is a basic webpack + THREE.js starter repository. It provides the following configuration:
- ES6 -> ES5 transpiling
- Webpack dev server
- HtmlWebpackPlugin
- CSS Extraction

## Getting Started
After cloning the repository run `npm install`.

Once complete, start a dev server using `npm run serve` or build for production using `npm run build`.

## Styles
Currently this repository only supports CSS. You can edit styles for the production build using `styles/main.css` and styles only for the development site using `styles/main.dev.css`.

## Assets
You can store files in the `assets` directory and reference them using `import` or `require`. If you need the URL of an asset use this format:

```
const texture = require('../assets/textures/grass.jpg');
const textureUrl = texture.default;
```

## Next Steps
- SASS configuration
- The CSS extraction configuration, if set to `[name].css` always seems to output the file as `main.css` even if that's not the original filename. This needs to be fixed. For now the name is hardcoded in `webpack.config.js`.